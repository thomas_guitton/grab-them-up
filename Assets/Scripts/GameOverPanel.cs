﻿using UnityEngine;
using UnityEngine.UI;


public class GameOverPanel : MonoBehaviour
{
    [SerializeField] private TMPro.TMP_Text _scoreText;
    [SerializeField] private TMPro.TMP_Text _highscoreText;


    private void OnEnable()
    {
        _scoreText.text = Score.score.ToString();
        _highscoreText.text = PlayerPrefs.GetInt("Highscore", 0).ToString();
    }
}
