﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenBoudaries : MonoBehaviour
{
    private float _minX, _maxX, _minY, _maxY;

    void Update()
    {
        float camDistance = Vector3.Distance(transform.position, Camera.main.transform.position);
        Vector2 bottomCorner = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, camDistance));
        Vector2 topCorner = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, camDistance));

        _minX = bottomCorner.x;
        _maxX = topCorner.x;
        _minY = bottomCorner.y;
        _maxY = topCorner.y;

        Vector3 pos = transform.position;

        if (gameObject.tag == "LeftWall")
        {
            pos.x = _minX;
            pos.y = 0;
            transform.localScale = new Vector3(1, _maxY - _minY, 1);
        }
        if (gameObject.tag == "RightWall")
        {
            pos.x = _maxX;
            pos.y = 0;
            transform.localScale = new Vector3(1, _maxY - _minY, 1);
        }
        if (gameObject.tag == "BottomWall")
        {
            pos.x = 0;
            pos.y = _minY;
            transform.localScale = new Vector3(_maxX - _minX, 1, 1);
        }
        if (gameObject.tag == "TopWall")
        {
            pos.x = 0;
            pos.y = _maxY;
            transform.localScale = new Vector3(_maxX - _minX, 1, 1);
        }
        transform.position = pos;
    }
}
