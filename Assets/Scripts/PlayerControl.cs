﻿using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    public static float playerSpeed = 8f;
    [SerializeField] private float _RoF;
    [SerializeField] private GameObject _hook;

    [SerializeField] private Animator _animator;

    void Start()
    {
        Physics2D.IgnoreLayerCollision(10, 8);
    }

    void FixedUpdate()
    {
        playerSpeed = Enemy.enemySpeed + 5;
        if (!GameManager.gameHasEnded)
        {
            if (Input.GetKey(KeyCode.Z))
            {
                transform.Translate(0, playerSpeed * Time.deltaTime, 0f, Space.World);
            }
            if (Input.GetKey(KeyCode.Q))
            {
                transform.Translate(-playerSpeed * Time.deltaTime, 0f, 0f, Space.World);
            }
            if (Input.GetKey(KeyCode.S))
            {
                transform.Translate(0, -playerSpeed * Time.deltaTime, 0f, Space.World);
            }
            if (Input.GetKey(KeyCode.D))
            {
                transform.Translate(playerSpeed * Time.deltaTime, 0f, 0f, Space.World);
            }

            LookMouse();

            if (Input.GetMouseButtonDown(0) && _hook.GetComponent<Hook>().hookIsReady)
            {
                _animator.SetBool("Shoot", true);
            }

        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Enemy" || other.gameObject.tag == "Boss")

        {
            FindObjectOfType<GameManager>().GameOver();
        }
    }

    void LookMouse()
    {
        Vector3 mousePosition = Input.mousePosition;
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);

        Vector2 direction = new Vector2(mousePosition.x - transform.position.x, mousePosition.y - transform.position.y);
        transform.up = direction;
    }

    void ShootHook()
    {
        _animator.SetBool("Shoot", false);
        _hook.SetActive(true);
    }
}
