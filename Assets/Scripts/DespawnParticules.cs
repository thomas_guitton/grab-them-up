﻿using UnityEngine;

public class DespawnParticules : MonoBehaviour
{
    [SerializeField] private ParticleSystem _ps;

    public void Start()
    {
    }

    public void Update()
    {
        if (_ps)
        {
            if (!_ps.IsAlive())
            {
                Destroy(_ps.gameObject);
            }
        }
    }
}
