﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static bool gameHasEnded = false;
    [SerializeField] private GameObject _gameOverPanel;
    [SerializeField] private GameObject _score;
    [SerializeField] private GameObject _highscore;

    [SerializeField] private float _slowDownFactor = 0.05f;
    [SerializeField] private float _slowDownLength = 0.15f;

    private float _originalEnemySpeed = 3f;
    private float _originalPlayerSpeed = 8f;

    void Update()
    {
        if (!PauseMenu.isPaused)
        {
            Time.timeScale += (1f / _slowDownLength) * Time.unscaledDeltaTime;
            Time.timeScale = Mathf.Clamp(Time.timeScale, 0f, 1f);
        }
    }

    public void GameOver()
    {
        if (!gameHasEnded)
        {
            gameHasEnded = true;
            _gameOverPanel.SetActive(true);
            _score.SetActive(false);
            _highscore.SetActive(false);
        }
    }

    public void Restart()
    {
        Score.score = 0;
        PlayerControl.playerSpeed = _originalPlayerSpeed;
        Enemy.enemySpeed = _originalEnemySpeed;
        gameHasEnded = false;
        EnemySpawner.enemyPool.Clear();
        EnemySpawner.bossPool.Clear();
        EnemySpawner.counterBoss = 0;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void QuitToMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void SlowMo()
    {
        Time.timeScale = _slowDownFactor;
        Time.fixedDeltaTime = Time.timeScale * .02f;
    }
}
