﻿using System.Collections;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    [SerializeField] private float _duration;
    [SerializeField] private float _magnitude;

    void Start()
    {
    }
    public IEnumerator Shake()
    {
        Vector3 originalPosition = transform.position;
        float elapsed = 0;
        while (elapsed < _duration)
        {
            float x = Random.Range(-1, 1) * _magnitude;
            float y = Random.Range(-1, 1) * _magnitude;

            transform.localPosition = new Vector3(x, y, originalPosition.z);

            elapsed += Time.deltaTime;
            yield return null;
        }

        transform.localPosition = originalPosition;
    }
}
