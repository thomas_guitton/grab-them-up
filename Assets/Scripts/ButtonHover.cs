﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonHover : MonoBehaviour
{
    private AudioManager _audioManager;

    void Start()
    {
        _audioManager = Object.FindObjectOfType<AudioManager>();
    }
    public void Hover()
    {
        _audioManager.Play("Hover");
    }
}