﻿using System.Collections;
using UnityEngine.Audio;
using System;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField] private AudioMixer _audioMixer;
    [SerializeField] private AudioMixerGroup _mainThemeGroup;
    [SerializeField] private AudioMixerGroup _uiGroup;
    [SerializeField] private AudioMixerGroup _collisionGroup;
    [SerializeField] private AudioMixerGroup _explosionGroup;

    public Sound[] sounds;
    public static AudioManager instance;

    [SerializeField] private float _transitionFilterTime;

    void Awake()
    {

        if (instance == null)
        {
            instance = this;
        }
        else if (gameObject.layer != 10)
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;

            if (s.name == "MainTheme")
            {
                s.source.outputAudioMixerGroup = _mainThemeGroup;
            }
            else if (s.name == "Hover")
            {
                s.source.outputAudioMixerGroup = _uiGroup;
            }
            else if (s.name == "Collision")
            {
                s.source.outputAudioMixerGroup = _collisionGroup;
            }
            else if (s.name == "Explosion")
            {
                s.source.outputAudioMixerGroup = _explosionGroup;
            }
        }

    }

    void Start()
    {
        Play("MainTheme");
    }

    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            return;
        }
        s.source.Play();
    }

    public void Stop(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            return;
        }
        s.source.Stop();
    }
    public void SetLowPassFilter(float value)
    {
        _audioMixer.SetFloat("Cutoff Freq", value);
    }
}
