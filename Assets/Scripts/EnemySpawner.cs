﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private Enemy _enemy;
    public static readonly List<Enemy> enemyPool = new List<Enemy>();

    [SerializeField] private Boss _boss;
    public static readonly List<Boss> bossPool = new List<Boss>();

    [SerializeField] private float _minDist;
    [SerializeField] private GameObject _player;

    [SerializeField] private int _spawnBossEvery = 20;
    public static int counterBoss = 0;

    private float _width = Screen.width;
    private float _height = Screen.height;

    void Start()
    {
        StartCoroutine(Respawn());
    }
    private IEnumerator Respawn()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            float spawnX = Random.Range
                            (Camera.main.ScreenToWorldPoint(new Vector2(0, 0)).x, Camera.main.ScreenToWorldPoint(new Vector2(_width, 0)).x);
            float spawnY = Random.Range
                (Camera.main.ScreenToWorldPoint(new Vector2(0, 0)).y, Camera.main.ScreenToWorldPoint(new Vector2(0, _height)).y);

            Vector2 spawnPosition = new Vector2(spawnX, spawnY);
            while (Vector2.Distance(spawnPosition, _player.transform.position) < _minDist)
            {
                spawnX = Random.Range
                (Camera.main.ScreenToWorldPoint(new Vector2(0, 0)).x, Camera.main.ScreenToWorldPoint(new Vector2(_width, 0)).x);
                spawnY = Random.Range
                    (Camera.main.ScreenToWorldPoint(new Vector2(0, 0)).y, Camera.main.ScreenToWorldPoint(new Vector2(0, _height)).y);

                spawnPosition = new Vector2(spawnX, spawnY);
            }
            var pixelPos = new Vector3(spawnX, spawnY, 0f);
            if (counterBoss >= _spawnBossEvery)
            {
                counterBoss = 0;
                var newEnemy = InstantiateBoss(_boss, pixelPos, Quaternion.identity);
                newEnemy.gameObject.SetActive(true);
            }
            else
            {
                var newBoss = InstantiateEnemy(_enemy, pixelPos, Quaternion.identity);
                newBoss.gameObject.SetActive(true);
            }

        }
    }

    private Enemy InstantiateEnemy(Enemy original, Vector3 position, Quaternion rotation)
    {
        if (enemyPool.Count > 0)
        {
            var enemy = enemyPool[0];
            enemyPool.RemoveAt(0);
            enemy.transform.position = position;
            enemy.transform.rotation = rotation;
            enemy.gameObject.SetActive(true);
            return enemy;
        }
        return Instantiate(original, position, rotation);
    }

    private Boss InstantiateBoss(Boss original, Vector3 position, Quaternion rotation)
    {
        if (bossPool.Count > 0)
        {
            var boss = bossPool[0];
            bossPool.RemoveAt(0);
            boss.transform.position = position;
            boss.transform.rotation = rotation;
            boss.gameObject.SetActive(true);
            return boss;
        }
        return Instantiate(original, position, rotation);
    }
}
