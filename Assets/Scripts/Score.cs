﻿using TMPro;
using UnityEngine;

public class Score : MonoBehaviour
{
    public static int score = 0;
    private int _MScore;

    [SerializeField] private TMP_Text _scoreText;
    [SerializeField] private TMP_Text _highscoreText;

    [SerializeField] private Animator _scoreAnim;
    [SerializeField] private Animator _highscoreAnim;

    void Start()
    {
        _MScore = score;
        _scoreText.text = score.ToString();
        _scoreAnim = GetComponent<Animator>();
        _highscoreAnim = _highscoreText.gameObject.GetComponent<Animator>();
    }

    void Update()
    {
        if (_MScore != score)
        {
            _scoreAnim.Play("ScoreAnimation", -1, 0f);
            _MScore = score;
            _scoreText.text = score.ToString();
        }

        if (score > PlayerPrefs.GetInt("Highscore", 0))
        {
            _highscoreAnim.Play("ScoreAnimation", -1, 0f);
            PlayerPrefs.SetInt("Highscore", score);
            PlayerPrefs.SetInt("Highscore", score);
        }

        _highscoreText.text = PlayerPrefs.GetInt("Highscore", 0).ToString();
    }
}
