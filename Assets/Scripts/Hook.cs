﻿using UnityEngine;

public class Hook : MonoBehaviour
{
    private Vector3 _initPosition;
    [SerializeField] private float _hookSpeed;
    [SerializeField] private float _hookForce;
    [SerializeField] private float _maxHookSize;
    private FixedJoint2D _joint;

    private float _currentSize;
    private bool _hooked = false;
    public bool hookIsReady = true;

    [SerializeField] private GameObject _player;
    private GameObject _enemy;

    private void OnEnable()
    {
        hookIsReady = false;
        Physics2D.IgnoreCollision(_player.GetComponent<Collider2D>(), GetComponent<Collider2D>());

        _joint = gameObject.GetComponent<FixedJoint2D>();

        transform.position = _player.transform.position;

        Vector3 mousePosition = Input.mousePosition;
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);

        Vector2 direction = new Vector2(mousePosition.x - transform.position.x, mousePosition.y - transform.position.y);
        transform.up = direction;

        _initPosition = transform.position;
    }

    private void Update()
    {
        _currentSize = Vector3.Distance(_initPosition, transform.position);
        if (!_hooked)
        {
            if (_currentSize <= _maxHookSize)
            {
                transform.Translate(0f, _hookSpeed * Time.deltaTime, 0f);
                _currentSize = Vector3.Distance(_initPosition, transform.position);
            }
            else EndHook();
        }
        else
        {
            transform.position = Vector2.MoveTowards(transform.position, _player.transform.position, 3 * _hookSpeed * Time.deltaTime);
        }

        if (_hooked && hasReachedPlayer())
        {
            _hooked = false;
            _enemy.GetComponent<Enemy>().enemyHooked = false;

            _joint.connectedBody = null;
            _enemy.GetComponent<Rigidbody2D>().AddForce(_player.transform.up * -_hookSpeed * _hookForce, ForceMode2D.Impulse);
            EndHook();
        }
    }

    private void EndHook()
    {
        hookIsReady = true;
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            FindObjectOfType<GameManager>().SlowMo();

            _enemy = other.gameObject;

            _joint.connectedBody = other.GetComponent<Rigidbody2D>();

            _hooked = true;
            other.GetComponent<Enemy>().enemyHooked = true;
        }
    }

    private bool hasReachedPlayer()
    {
        return Vector2.Distance(transform.position, _player.transform.position) <= 0.01;
    }
}