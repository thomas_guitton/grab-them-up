﻿using UnityEngine;

public class Tuto : MonoBehaviour
{
    void Start()
    {
        if (PlayerPrefs.GetInt("Highscore", 0) == 0)
        {
            gameObject.SetActive(true);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
}
