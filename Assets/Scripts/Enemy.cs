﻿using System.Collections;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public static float enemySpeed = 3f;
    [SerializeField] private float _accelerateFactor;
    public bool enemyHooked;

    [SerializeField] private Color _startColor;
    [SerializeField] private GameObject _deathParticles;
    [SerializeField] private GameObject _collisionParticles;
    [SerializeField] private Animator _animator;

    private GameObject _player;
    private CameraShake _cameraShake;
    private AudioManager _audioManager;


    private void OnEnable()
    {
        _player = GameObject.FindGameObjectsWithTag("Player")[0];
        _cameraShake = FindObjectOfType<CameraShake>();
        _audioManager = FindObjectOfType<AudioManager>();

        gameObject.layer = 9;
    }

    void Update()
    {
        if (enemySpeed < 5)
        {
            enemySpeed += _accelerateFactor;
        }
        if (!_animator.GetBool("IsDead"))
        {
            transform.position = Vector2.MoveTowards(transform.position, _player.transform.position, enemySpeed * Time.deltaTime);
        }
        if (enemyHooked)
            StartCoroutine(TurnHarmless());
    }

    IEnumerator TurnHarmless()
    {
        gameObject.layer = 8;
        enemyHooked = false;
        yield return new WaitForSeconds(1f);
        gameObject.layer = 9;
    }


    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            if (gameObject.layer == 8 || other.gameObject.layer == 8)
            {
                gameObject.layer = 8;
                _animator.SetBool("IsDead", true);
                Instantiate(_collisionParticles, transform.position, Quaternion.identity);
            }
        }
        if (other.gameObject.tag == "Boss" && !_animator.GetBool("IsDead"))
        {
            if (gameObject.layer == 8)
            {
                _animator.SetBool("IsDead", true);
                Instantiate(_collisionParticles, transform.position, Quaternion.identity);
                StartCoroutine(other.gameObject.GetComponentInParent<Boss>().TakeDamage());
            }
        }
        else if (other.gameObject.tag == "LeftWall" || other.gameObject.tag == "RightWall" || other.gameObject.tag == "BottomWall" || other.gameObject.tag == "TopWall")
        {
            if (gameObject.layer == 8)
            {
                _animator.SetBool("IsDead", true);
                Instantiate(_collisionParticles, transform.position, Quaternion.identity);
            }
        }

    }

    public void PlayColisionSound()
    {
        _audioManager.Play("Collision");
    }

    public void ShakeCamera()
    {
        StartCoroutine(_cameraShake.Shake());
    }

    public void DestroyEnemy()
    {
        Instantiate(_deathParticles, transform.position, Quaternion.identity);
        _audioManager.Play("Explosion");
        Score.score++;
        EnemySpawner.counterBoss++;

        GetComponentInChildren<SpriteRenderer>().color = _startColor;
        gameObject.SetActive(false);
        EnemySpawner.enemyPool.Add(this);
    }
}
