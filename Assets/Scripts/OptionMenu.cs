﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class OptionMenu : MonoBehaviour
{
    [SerializeField] private AudioMixer _audioMixer;
    [SerializeField] private TMPro.TMP_Dropdown _resolutionDropDown;
    private Resolution[] _resolutions;

    private bool _isMute;

    [SerializeField] private Slider _volumeSlider;
    [SerializeField] private Toggle _muteToggle;

    void Start()
    {
        _resolutions = Screen.resolutions;
        _resolutionDropDown.ClearOptions();
        List<string> options = new List<string>();

        int currentResolutionIndex = 0;
        for (int i = 0; i < _resolutions.Length; i++)
        {
            string option = _resolutions[i].width + "x" + _resolutions[i].height;
            options.Add(option);

            if (_resolutions[i].width == Screen.currentResolution.width && _resolutions[i].height == Screen.currentResolution.height)
            {
                currentResolutionIndex = i;
            }
        }

        _resolutionDropDown.AddOptions(options);
        _resolutionDropDown.value = currentResolutionIndex;
        _resolutionDropDown.RefreshShownValue();
    }

    private void OnEnable()
    {
        _volumeSlider.value = PlayerPrefs.GetFloat("Volume", 0);
        _muteToggle.isOn = (PlayerPrefs.GetInt("IsMuted", 0) == 1);
    }

    void Update()
    {
        if (PlayerPrefs.GetInt("IsMuted", 0) != 1)
        {
            _audioMixer.SetFloat("MasterVolume", PlayerPrefs.GetFloat("Volume", 0));
        }
    }
    public void SetVolume(float volume)
    {
        PlayerPrefs.SetFloat("Volume", volume);
    }

    public void SetQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    public void SetResolution(int resolutionIndex)
    {
        Resolution resolution = _resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

    public void SetFullScreen(bool isFullScreen)
    {
        Screen.fullScreen = isFullScreen;
    }

    public void ResetHighscore()
    {
        PlayerPrefs.SetInt("Highscore", 0);
    }

    public void Mute(bool value)
    {
        if (value)
        {
            PlayerPrefs.SetInt("IsMuted", 1);
            _audioMixer.SetFloat("MasterVolume", -80);
        }
        else
        {
            PlayerPrefs.SetInt("IsMuted", 0);
            _audioMixer.SetFloat("MasterVolume", PlayerPrefs.GetFloat("Volume", 0));
        }
    }
}