﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] private AudioManager _audioManager;
    [SerializeField] private GameObject _pauseMenuUI;

    public static bool isPaused = false;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !GameManager.gameHasEnded)
        {
            if (isPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    void Pause()
    {
        _pauseMenuUI.SetActive(true);
        Time.timeScale = 0;
        isPaused = true;

        _audioManager.SetLowPassFilter(1000);
    }

    public void Resume()
    {
        _pauseMenuUI.SetActive(false);
        Time.timeScale = 1;
        isPaused = false;

        _audioManager.SetLowPassFilter(15000);
    }

    public void LoadMenu()
    {
        gameObject.SetActive(false);
        Time.timeScale = 1f;
        isPaused = false;
        _audioManager.SetLowPassFilter(15000);
        SceneManager.LoadScene("MainMenu");
    }
}
