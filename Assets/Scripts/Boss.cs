﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Boss : MonoBehaviour
{
    [SerializeField] private float _bossSpeed;
    [SerializeField] int _hpMax;
    [SerializeField] private Color _startColor;
    [HideInInspector]
    public int hp;

    [SerializeField] private TMP_Text _hpText;
    [SerializeField] private Collider2D _collider;
    [SerializeField] private Animator _animator;
    [SerializeField] GameObject _particleSys;

    private GameObject _player;
    private CameraShake _cameraShake;


    private void OnEnable()
    {
        _player = GameObject.FindGameObjectWithTag("Player");
        _cameraShake = FindObjectOfType<CameraShake>();

        gameObject.layer = 12;
        hp = _hpMax;
    }

    void Update()
    {
        _hpText.text = hp.ToString() + "/" + _hpMax.ToString();

        if (!_animator.GetBool("IsDead"))
        {
            transform.position = Vector2.MoveTowards(transform.position, _player.transform.position, _bossSpeed * Time.deltaTime);
        }

        if (hp <= 0)
        {
            hp = 0;
            _animator.SetBool("IsDead", true);
            gameObject.layer = 8;
        }
    }
    public void Land()
    {
        StartCoroutine(_cameraShake.Shake());
        _collider.enabled = true;
    }


    public IEnumerator TakeDamage()
    {
        hp--;
        _animator.SetBool("TakeDamage", true);
        yield return new WaitForSeconds(0.2f);
        _animator.SetBool("TakeDamage", false);

    }

    public void DestroyBoss()
    {
        Instantiate(_particleSys, transform.position, Quaternion.identity);
        Score.score += 5;
        EnemySpawner.counterBoss += 5;

        _collider.enabled = false;
        GetComponentInChildren<SpriteRenderer>().color = _startColor;
        gameObject.SetActive(false);
        EnemySpawner.bossPool.Add(this);
    }
}
